# create import busio
import busio
import digitalio
import board
import threading
import time
import datetime
from gpiozero import Button
import os
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn

# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

# create an analog input channel on pin 1 and pin 2
#channles handle the LDR and the MCP9700
ldr = AnalogIn(mcp, MCP.P2)
temp  = AnalogIn(mcp, MCP.P1)

# setup button
button = Button(21, bounce_time=0.2)

running = True
int = 1

#Function that prints out the measured and converted values in the desired output format
#runtime is calculateed by determining the start time of the function and then removing this from the runtime of the loop. it does this on each iteration to ensure that the loop increments in the specific intervals
def printOutputs():
        global running, int
        sTime = datetime.datetime.now().second
        os.system('clear')
        print("Runtime\t\tTemp Reading\t\tTemp\t\tLight Reading")
        while running:
                rTime = datetime.datetime.now().second - startTime
                if runTime >= interval:
                        os.system('clear')
                        c_temp  = round((temp.voltage-0.5)/0.01, 2)                             
                        print('{}s\t\t{}\t\t\t{}\t\t{}'.format(rTime, temp.value, c_temp, ldr.value))
                        sTime = datetime.datetime.now().secon

                       
                       

#when the button is pressed this function is run increasing the interval value from 1 to 5 to 10 and back to 1 indenitiely. 
def incIntLength():
        global int
        if int == 1:
                int = 5
        elif int == 5:
                int = 10
        else:
                int = 1

#the main function of the code that initiates the threading then runs the printing and reading of the values from the LDR and thermosister simulatanouesly. 
#the code alos handles the cases of a keybaord interupt (ctrl+c) and if an unexpected error occurs. By rethreathing the program and then exiting
if __name__ == "__main__":
        x = threading.Thread(target=printOutputs)
        x.start()
        button.when_pressed = incIntLength
        try:
                while True:
                        pass
        except KeyboardInterrupt:
                print("Rethreading and exciting")
                running = False
                x.join()
        except e:
                print("An error occurred: {}".format(e.message))
