//the ALU testbench module
module ALU_tb();    

	//inputs
	reg clk;
	reg[7:0] A,B;
	reg[3:0] ALU_opc;
  
	// output
    reg [7:0] Acc; 

//Instantiate the design under test
   reg[7:0] answer;
  
ALU test_unit(
   .clk(clk),
   .A(A),.B(B),  // ALU 8-bit Inputs
   .ALU_opc(ALU_opc),// ALU Selection
   .Acc(Acc) // ALU 8-bit Output
);

initial begin //Initial means this only happens once
    $display("A  B  ALU_opc  Acc");
    $monitor("%b  %b  %b     %b",A,B,ALU_opc, Acc);
    $dumpfile("dump.vcd");
    $dumpvars;
  
    // ADD
    A = 8'b0001;
    B = 8'b0100;
    ALU_opc = 4'b0000;
    clk=1'b1;
    #5;
    answer = A + B;
    clk=1'b0;
    #5;

    // MAC
    A = 8'b0010;
    B = 8'b0011;
    ALU_opc = 4'b0110;
    clk=1'b1;
    #5;
    answer = 8'b0101 + (A * B);
    clk=1'b0;
    #5;
    
    // ROR
    A = 8'b0011;
    ALU_opc = 4'b1000;
    clk=1'b1;
    #5;
    answer = 8'b10000001;
    clk=1'b0;
    #5;
  
    // LTH
    A = 8'b0011;
    B = 8'b0010;
    ALU_opc = 4'b1111;
    clk=1'b1;
    #5;
    answer = 8'h0;
    clk=1'b0;
    #5;

  end
endmodule
