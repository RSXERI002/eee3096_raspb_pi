# Import libraries
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os
import time
from gpiozero import PWMLED

# some global variables that need to change as we run the program
end_of_game = None  # set if the user wins or ends the game
ACC_LED = None
BUZZER = None
scores = []
pressed = 0 
answer = 0 




# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]
LED_accuracy = 32
btn_submit = 16
btn_increase = 18
buzzer = 33
eeprom = ES2EEPROMUtils.ES2EEPROM()


# Print the game banner
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


# Print the game menu
def menu():
    global end_of_game, answer
    
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    elif option == "P":
        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        answer = generate_number()
        while not end_of_game:
            pass
    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")


def display_scores(count, raw_data):
    # print the scores to the screen in the expected format
    print("There are {} scores. Here are the top 3!".format(count))
    # print out the scores in the required format
    index =1
    for score in raw_data:
        print("{} - {} took {} guesses".format(index,score[0], score[1]))
        index += 1
    pass


# Setup Pins
def setup():
    global BUZZER, ACC_LED
    # Setup board mode
    GPIO.setmode(GPIO.BOARD)

    # Setup regular GPIO
    GPIO.setup(buzzer, GPIO.OUT)
    GPIO.setup(LED_accuracy, GPIO.OUT)
    GPIO.setup(btn_submit, GPIO.IN, pull_up_down = GPIO.PUD_UP) #buttons open initialy
    GPIO.setup(btn_increase, GPIO.IN, pull_up_down = GPIO.PUD_UP) #buttons open initialy
   
    # Setup PWM channels
  

    if BUZZER is None:
        BUZZER = GPIO.PWM(buzzer, 1000) # sets PWM for buzzer 
        BUZZER.start(0) 
    
    if ACC_LED is None:
        ACC_LED = GPIO.PWM(LED_accuracy, 50)
        ACC_LED.start(0)

    # Setup debouncing and callbacks
    GPIO.add_event_detect(btn_increase,GPIO.FALLING, callback=btn_increase_pressed, bouncetime = 500) # bouncetime makes sure it isnt detecting noise
    GPIO.add_event_detect(btn_submit,GPIO.FALLING, callback=btn_guess_pressed, bouncetime = 500) #rising edge detection of button pressed 

    for PINled in LED_value:
        GPIO.setup(PINled, GPIO.OUT) #setup as output pin 
        GPIO.output(PINled, GPIO.LOW) #output initially low 
    pass


# Load high scores
def fetch_scores():
    # get however many scores there are
    score_count = None
    # Get the scores
    
    # convert the codes back to ascii
    
    # return back the results
    return score_count, scores


# Save high scores
def save_scores():
    # fetch scores
    # include new score
    # sort
    # update total amount of scores
    # write new scores
    pass


# Generate guess number
def generate_number():
    return random.randint(0, pow(2, 3)-1) # range 0 - 7


# Increase button pressed
def btn_increase_pressed(channel):
    # Increase the value shown on the LEDs
    # You can choose to have a global variable store the user's current guess, 
    # or just pull the value off the LEDs when a user makes a guess
    global pressed
    ButtonValue = GPIO.input(btn_increase)

    if ButtonValue == 0: #button low therefore being pressed 
        if pressed == 7:
            pressed = 0 
        else:
            pressed += 1 
    print(pressed)
    if pressed ==0:
        GPIO.output(LED_value[0], GPIO.LOW)
        GPIO.output(LED_value[1], GPIO.LOW)
        GPIO.output(LED_value[2], GPIO.LOW)
    
    elif pressed == 1: 
        GPIO.output(LED_value[0], GPIO.LOW)
        GPIO.output(LED_value[1], GPIO.LOW)
        GPIO.output(LED_value[2], GPIO.HIGH)

    elif pressed == 2: 
        GPIO.output(LED_value[0], GPIO.LOW)
        GPIO.output(LED_value[1], GPIO.HIGH)
        GPIO.output(LED_value[2], GPIO.LOW)
    
    elif pressed == 3: 
        GPIO.output(LED_value[0], GPIO.LOW)
        GPIO.output(LED_value[1], GPIO.HIGH)
        GPIO.output(LED_value[2], GPIO.HIGH)
    
    elif pressed == 4: 
        GPIO.output(LED_value[0], GPIO.HIGH)
        GPIO.output(LED_value[1], GPIO.LOW)
        GPIO.output(LED_value[2], GPIO.LOW)
    
    elif pressed == 5: 
        GPIO.output(LED_value[0], GPIO.HIGH)
        GPIO.output(LED_value[1], GPIO.LOW)
        GPIO.output(LED_value[2], GPIO.HIGH)
    
    elif pressed == 6: 
        GPIO.output(LED_value[0], GPIO.HIGH)
        GPIO.output(LED_value[1], GPIO.HIGH)
        GPIO.output(LED_value[2], GPIO.LOW)

    elif pressed == 7: 
        GPIO.output(LED_value[0], GPIO.HIGH)
        GPIO.output(LED_value[1], GPIO.HIGH)
        GPIO.output(LED_value[2], GPIO.HIGH)
    pass


# Guess button
def btn_guess_pressed(channel):
    # If they've pressed and held the button, clear up the GPIO and take them back to the menu screen
    # Compare the actual value with the user value displayed on the LEDs
    # Change the PWM LED
    # if it's close enough, adjust the buzzer
    # if it's an exact guess:
    # - Disable LEDs and Buzzer
    # - tell the user and prompt them for a name
    # - fetch all the scores
    # - add the new score
    # - sort the scores
    # - Store the scores back to the EEPROM, being sure to update the score count

    global pressed, answer #your answer and there answer 
    pressLength = time.time()
    Menu = False 
    ButtonState = GPIO.LOW

    #back to menu 
    while GPIO.input(btn_submit)== ButtonState:
        time.sleep(0.05)
        length = time.time() - pressLength

        if length >1:
            print("Starting again...")
            GPIO.cleanup()
            setup()
            menu()
            Menu = True 
            break 
    #actual value compared to user value 
    print("You guessed:",pressed) 
    print("The correct answer was:",answer)
    if pressed != answer and not Menu:
        accuracy_leds()
        trigger_buzzer()
    elif pressed == answer and not Menu:
        GPIO.cleanup()
        name = input("Well done you guessed correctly. Enter your name:")
        name = name.upper()
        
        if len(name)>3:
            name = name[0:3]
        print(name)
        GPIO.output(LED_value[0], GPIO.LOW)
        GPIO.output(LED_value[1], GPIO.LOW)
        GPIO.output(LED_value[2], GPIO.LOW)
        pressed = 0
        ##############################################################


    pass


# LED Brightness
def accuracy_leds():
    # Set the brightness of the LED based on how close the guess is to the answer
    # - The % brightness should be directly proportional to the % "closeness"
    # - For example if the answer is 6 and a user guesses 4, the brightness should be at 4/6*100 = 66%
    # - If they guessed 7, the brightness would be at ((8-7)/(8-6)*100 = 50%
    global pressed, ACC_LED, answer
    temp_pressed = pressed
    temp_answer = answer

    #formula needs /by and cant /0 so then change temp_answer
    if temp_answer ==0:
        temp_answer =1
        temp_pressed = pressed +1 
    
    if temp_answer>temp_pressed: #case where users guess is less than answer 
        brightness = (pressed/answer)*100
        ACC_LED.start(brightness)
    else:
        duty = (8 - temp_pressed)/(8-temp_answer) #for when users input greater than answer 
        # duty needs to be 0<duty<1
        if duty>1 or duty<0:
            duty = 0 
        duty = duty*100
        ACC_LED.start(duty) 

    pass

# Sound Buzzer
def trigger_buzzer():
    # The buzzer operates differently from the LED
    # While we want the brightness of the LED to change(duty cycle), we want the frequency of the buzzer to change
    # The buzzer duty cycle should be left at 50%
    # If the user is off by an absolute value of 3, the buzzer should sound once every second
    # If the user is off by an absolute value of 2, the buzzer should sound twice every second
    # If the user is off by an absolute value of 1, the buzzer should sound 4 times a second

    global BUZZER, answer, pressed, btn_submit
    difference = abs(answer - pressed)
   
    
    if (difference == 1) and  (btn_submit == True):
        GPIO.output(BUZZER, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(BUZZER, GPIO.LOW)
    elif (difference == 2) and (btn_submit == True):
        GPIO.output(BUZZER, GPIO.HIGH)
        time.sleep(2)
        GPIO.output(BUZZER, GPIO.LOW
    elif (difference == 3) and (btn_submit == True):
        GPIO.output(BUZZER, GPIO.HIGH)
        time.sleep(3)
        GPIO.output(BUZZER, GPIO.LOW)
    
    pass


if __name__ == "__main__":
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
